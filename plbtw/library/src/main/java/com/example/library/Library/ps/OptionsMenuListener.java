package com.example.library.Library.ps;

import android.view.MenuItem;

/**
 * Created by nyoman on 5/20/2016.
 */
public interface OptionsMenuListener {
    void onOptionsItemSelected(MenuItem item);
}
